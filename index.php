<html>
<head>
<meta charset="utf-8">
<title>Bienvenue sur notre site</title>
<link rel="stylesheet" type="text/css" href="view/css/bootstrap.css" />
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700'
	rel='stylesheet' type='text/css'>

<link rel="stylesheet" type="text/css" media="all" href="view/css/style.css">

<script src="view/js/jquery-1.11.2.js"></script>
<script src="view/js/bootstrap.js"></script>
<script src="view/js/javascript.js"></script>
</head>
<body>
<?php
require 'view/top-header.html';
require 'view/header.html';
require 'view/slider.html';
require 'view/content.html';
require 'view/footer.html';
?>

</body>
</html>