$('a[href="#"]').on('click', function(e) {
	e.preventDefault();
});

$('#menu > li').on('mouseover', function(e) {
	$(this).find("ul:first").show();
	$(this).find('> a').addClass('active');
}).on('mouseout', function(e) {
	$(this).find("ul:first").hide();
	$(this).find('> a').removeClass('active');
});

$('#menu li li').on('mouseover',function(e){
	  if($(this).has('ul').length) {
	    $(this).parent().addClass('expanded');
	  }
	  $('ul:first',this).parent().find('> a').addClass('active');
	  $('ul:first',this).show();
	}).on('mouseout',function(e){
	  $(this).parent().removeClass('expanded');
	  $('ul:first',this).parent().find('> a').removeClass('active');
	  $('ul:first', this).hide();
	});

$(document).ready(function() {
	$('.scrollTo').click( function() { // Au clic sur un élément
		var page = $(this).attr('href'); // Page cible
		var speed = 750; // Durée de l'animation (en ms)
		$('html, body').animate( { scrollTop: $(page).offset().top }, speed ); // Go
		return false;
	});
});

var oritop = -20;
var leftActive = true;
$(window).scroll(function() {
	var scrollt = window.scrollY;
	var elm = $("#menu");
	if (oritop < 0) {
		oritop = elm.offset().top;
	}
	if (scrollt >= oritop - 20) {
		elm.css({
			"position" : "fixed",
			"top": 0,
			"left" : 0,
			"width" : "100%",
			"background-color": "#384958",
			"z-index": "9999"
		});
	} else {
		elm.css({
			"position" : "relative",
			"top" : "0em",
			"width" : "inherit"
		});
	}
	
});

